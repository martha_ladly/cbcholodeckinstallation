# CBC Newsroom Holodeck Installation
## Project done by OCAD U VALab

### Overview

>The CBC Newsworld Holodeck utilizes the GestureTek software system and Microsoft Kinect to sense the user’s gestures, enabling interaction with multi-screen video display. Video data communicates with the gesture-based library through an open-source C++ toolkit, which allows flexibility of video control, animation, and speed optimization. The Kinect searches for the user’s hand; upon detecting the gesture the system calls an action that corresponds to that gesture. The called action selects a video, plays that video, which initiates keywords to populate the screen, or hides them during video playback, enabling focused viewing.
 
> Keyword/phrase selection takes the viewer directly to the selected video clip (enabled by natural language processing technologies), a novel interaction in video browsing, search and display. The Holodeck interface enables gestural interaction, and rich, context-aware browsing and search of the CBC Newsworld ‘big data’ video corpus.

>Our aim is to pursue research in visualization and sonification of large portions of the CBC Newsworld corpus: the collected and digitized 24‐hour air‐check videos from the last 23 years (back to 1989), and, more generally, to enable spoken phrase and keyword search, information seeking, search and display and segment review within this corpus.

> [OCAD U Mobile Lab Research ](http://research.ocadu.ca/mobilelab/project/the-cbc-newsworld-holodeck)

Before the CBC Holodeck installation was started the CBC Holodeck web version was prototyped to confirm the concept. The repo for the web prototype is located [here](https://bitbucket.org/martha_ladly/cbcholodeckweb). 

The CBC Holodeck was developed over a number of years at OCAD University in the Visual Analytics Lab under direction of Phd. Martha Ladly.

### CBC Holodeck versions

In this repo there are two versions of the app. The apps are located in the `apps/myApps/ofNEWSGestDisplay_1018` and `apps/myApps/ofNEWSGestDisplay_030516`. The first app, `ofNEWSGestDisplay_1018` is the offical stable version of the app with a single day from 01/01/93. The second app `ofNEWSGestDisplay_030516` is where the introduction of the second day is located. This day is 01/01/07. The second day videos and keywords have been added here. As well, the toggle button for switching between the two days has been added in the top left of the projection.

### Installation Requirements

Technical requirements for the CBC Holodeck:

* The CBC Holodeck code base
* openFrameworks 0.8.1
* Codeblocks v 12.11
* AZRockey (included in the GestureTek)
* GestureTek Structured Light Software (CD)
  * GestureTek Tracking software (SOFTWARE APP NAME)
  * GestureTek Tracking Library
* OpenNI/NITE (optional)

Hardware requirements:

* Macbook Pro Retina with Windows Bootcamp
* GestureTek Licence USB key (The USB tag with the star on the tag is currently active)
* Microsoft Kenickt
* Speakers
* Projector
* DVI to VGA/HDMI dongles


### Installation

As of April 21st 2016 the CBC Holodeck is installed and in a stable state on the VALabs MacBook Pro on the Windows partition of the laptop.

##### GESTURETEK INSTALLATION

1. Make sure that the Kinect and the GestureTek USB License Key are plugged on your computer.2. Run setup.exe/autorun from the GestureTek CD.3. It will install the Microsoft Kinect OpenNI / NITE, GestureTek Structured Light and theAZRockey

##### CODEBLOCKS for OPENFRAMEWORKS INSTALLATION

**NOTE: Codeblocks is no longer supported by openFrameworks. We have the openFrameworks CodeBlocks legacy install on the MacBook Pro if it is required at any point in the future. If a re-write / refactoring takes place in the future I would recommend using the most recent version of openFrameworks (0.9+) and Visual Studio IDE.**

(Copied from the openFrameworks Code::Blocks IDE Setup Guide)We like Code::Blocks for Windows development since it's light weight, and avoids some of the quirkiness of windows visual studio. here's a step by step tutorial to get up and running. Note: [Step e](http://openframeworks.cc/setup/codeblocks/#additions)) is required to run openFrameworks. Please do not skip this step.
##### VERSIONThe new version of Code::Blocks (12.11) is not compatible with openFrameworks versions 0073 and lesser. It will work with 0.7.4 (forthcoming) and what's on the develop branch of github. This is because of a change in the compiler which makes libraries compiled with an older compiler incompatible with the newer compiler. If you are using openFrameworks 0073 or lesser, use [Code::Blocks 10.05](http://www.codeblocks.org/downloads/26).
##### INSTALLATION
1. Download Code::Blocks binary (latest release)Download Code::Blocks Note: download WITH MinGW. Code::Blocks version 12.11 works well with openFrameworks 0.7.4+.
   
   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_1.png)2. Install
   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_2.png)
   3. You don't have to change anything
   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_3.png)4. I chose not too, since Code::Blocks is not my primary IDE
   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_4.png)
   5. Add files to MinGW. Similar to devcpp, we will have to add a few libraries to devcpp. 

   You can download the files:[Additions for Code::Blocks (ZIP)](http://www.openframeworks.cc/content/files/codeblocks_additions.zip) to work with openFrameworks. 
   In the zip there are two folders, you need to put the contents of them into the contents of folders in MinGW.  a. Add the contents of the folder `add_to_codeblocks_mingw_include` into `C:\Program Files\CodeBlocks\MinGW\include` (or wherever your app\mingw\include is)  b. Add the contents of the folder `add_to_codeblocks_mingw_lib` into `C:\Program Files\CodeBlocks\MinGW\lib` (or wherever your app\mingw\lib is)![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_5.png)   These are additional libs and header files that need to be added to the MinGW distribution that comes with Code::Blocks.### CLONNING THE PROJECT
Instead of installing everything OpenFrameworks and the Holodeck source from scratch you can clone / download this repo.

To download the [zip file](https://bitbucket.org/martha_ladly/cbcholodeckinstallation/get/656523b60cab.zip) which will not include the projets git history that allows you to contribute to the future development of the project or you can clone the repo to you machine, contribute and commit improvements back to the project. To clone the project you will need Terminal (OSX) or PuTTY on Windows and have git installed on your machine -- will use the command `git clone git@bitbucket.org:martha_ladly/cbcholodeckinstallation.git` do clone this repo.

Once the project is downloaded or cloned you will need to add the archived CBC videos to the project. See the `Media` section of this README.md file for instructions. The reason the videos are not in the repo is that OCAD U and the VALab do not have permission to make the CBC videos public. 

** NOTE: DO NOT ADD THE CBC VIDEOS TO THIS REPO **

### RUNNING CBC NEWSWORLD HOLODECK SOURCE CODE

**HARDWARE SETUP**
1. Microsoft Kinect must be connected to the computer2. GestureTek USB License Key3. Must be connected to two projectors
**ENVIRONMENT SETUP**1. Extend displays. (If 3 displays are available, duplicate displays 1 and 2, and extend display 3)2. Both displays must be 1680 x 1050.   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_6.png)

   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/771a6b40b1ac9bd21c1ad8db25ae75394c4f48cb/reademeImages/install_6.5.png)   
**GestureTek**

1. Double click the GestTrack3D Multi Tracker on the Desktop2. Click on Start Tracker
   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_7.png)
**OPENING SOURCE CODE ON CODE::BLOCKS**
1. Click on the Code::Blocks icon on the Task Bar   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_8.png)
   2. Go to File ­> Open , or click on the Open Folder icon.3. Navigate to:`C:\Users\mxlab\Desktop\ofNEWSGestTekMultiDisplay_oct18\apps/myApps\ofNEWSGestDisplay_1018\ofNEWSGestDisplay_1018.workspace`￼￼
4. Select the ofNEWSDemo_apr18.workspace (workspace file)5. Click Open.

**RUNNING THE CBC NEWSWORLD HOLODECK**

   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/7fd0fce06b2ccbeead13398297a6936c0f346862/reademeImages/install_9.png)

1. Click on the Run icon.2. Or, go to Build ­> Run.


**CBC Holodeck Resolution Configuration**

1. open CodeBlocks, open FIRST file, named: C:\Users\mxlab\Desktop\ofNEWSGestTekMultiDisplay_oct18\apps/myApps\ofNEWSGestDisplay_1018\ofNEWSGestDisplay_1018.workspace
2. 3 files will open when you click that first file
3. click the file with the ending ofNEWSResources.h
4. within that file, you have to make sure the computer resolution matches what is written in the code. In this example, the computer and projector resolution was 1400x1050.
5. Adjust the settings according to this guide:
   * smallPlayerWidth is the total resolution width / 4
   * smallPlayerHeight is the total resolution height / 3
   * screenWidth is the resolution width x 2
   * screenHeight is the resolution height (in this example, it is 1050)
   * screenHalfWidth is the resolution width (in this example, it is 1400)
   
   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/771a6b40b1ac9bd21c1ad8db25ae75394c4f48cb/reademeImages/install_11.png)

6. click the file ending with ofVid.cpp
7. Scroll down to line 177, and adjust the case. That number should be double what the resolution width is. Ex, if the width is 1400, the case number should be 2800. Please note – this is sometimes finicky and does not always seem to matter. Sometimes you have to play around with different numbers to get it to work. See image below as reference:

   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/771a6b40b1ac9bd21c1ad8db25ae75394c4f48cb/reademeImages/install_12.png)

8. Click ‘Build and Run’ (located at the top in CodeBlocks) and you’re ready to go.

**Audio**

1. go to the audio icon in the bottom right corner of the computer, click the ‘audio’ button.
2. The audio should be set to ‘headphones (cirrus logic CS4206B (AB 40))’.
3. If this is not showing up, go to: Control Panel>Sound> and you might have to disable the Speakers and set it to the Headphones setting.

   ![image](https://bytebucket.org/martha_ladly/cbcholodeckinstallation/raw/771a6b40b1ac9bd21c1ad8db25ae75394c4f48cb/reademeImages/install_10.png)


**TROUBLESHOOTING AUDIO**

1. if audio is not working after selecting these settings, you have to reboot the RealTek audio.
2. To do this:
   * Go to your folders icon (on the bottom left row of the computer).
   * Click BOOTCAMP (C:)
   * search “realtek”.
   * Click the first RealTek folder>click RealTekSetup>
   * follow the directions to reboot the RealTek speakers.
 
**IMPORTANT NOTE:** 
After re-booting the speakers, you will be prompted to restart the computer – re-start it. After the computer re-starts, make sure the display settings have the correct resolution – the computer tends to revert to the default highest setting, which is usually not what you need.

### Interactions + Behaviour

**TODO:** get complete list of interactions and how to preform them here.

Here are a list of gestures that the user can use to interact with the CBC Holodeck. Be located within the installation, standing infront of and facing the Microsoft Kinect. When the Holodeck recignises you and is ready for interaction a blue curser will be displayed on the projection in the installation. This represents where your "mouse" is and where you are currently controlling.

* Navigation: Hold your hand out in front of you palm facing the Kinect. As you move your hand around the blue cursur will follow your hand around on the projection.
* Selecting: When you move the cursur over your a specific video and hover over the video. After a couple seconds the cursur will start to pulsate indicating a video selection and the opposite screen will display the selected video in full screen mode.
* Selecting a keyword: Once the selected video is in full screen mode, keyword text will start to appear over the fullscreen video. To select the text and skip ahead in the video to where that keyword is in the video navigate and hover over the desired keyword.
* Exit full screen mode: To exit full screen mode and return to the full video grid view make a "swipping" motion with your hands as if you are pushing the full screen video off of the screen. This will show the full video grid on both projections.


### Folder Structure

The repo includes the complete folder structure for OpenFrameworks as well as the CBC Holodeck app. This was done because the current version of OF that is being used is a legacy version and I wanted to make sure that the project would be able to have the framework available no matter what. 

The CBC Holodeck actual application is located at `apps/myApps/ofNEWSGestDisplay_1018` and the bulk of the source code is located in the folder `apps/myApps/ofNEWSGestDisplay_1018/src`. To control the resolution of the projects displays for different setups refer to the **CBC Holodeck Resolution Configuration** section above.

To learn more about how OpenFrameworks and to better understand how the library is structured there is exelent [documentation on their site](http://openframeworks.cc/) as well as a [great opensourced book](http://openframeworks.cc/ofBook/chapters/foreword.html).

**Media**

OpenFrameworks requires all media in a project to be located in the `apps/myApps/ofNEWSGestDisplay_1018/bin/data` folder. This is where the video assets should be placed as well as this is where the keyword text files and fonts are located for the project. 


### Media

**DO NOT ADD THE CBC VIDEOS TO THIS REPO**

The media video files for this project can be found on the hard-drive in the teck lockers in the VALab OR in the original project on the MacBook Pro.


### New Features

Here is a list of work in progress that is being done on the installtaion.

* there are some glitches while toggling between the two different days. Sometimes the toggling causes the installation to freeze. I suspect it is because of loading a whole new set of videos while the app is running. Perhaps making this more efficient, preloading the videos, or another solution is needed to made it complete.
* ~~adding a second day of videos to the installation~~
* ~~adding the ability to toggle between the two days while the installtion is running. ie. not having to restart the installation and make updates to the code to diplay a different day.~~

