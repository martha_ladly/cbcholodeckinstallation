#include "ofTextSelect.h"
#include "ofNEWSResources.h"

#include "ofMain.h"

//--------------------------------------------------------------
ofTextSelect::ofTextSelect(string filename, float vidDuration, int whichDay)
{
    // Initialize certain variables
    init(filename, vidDuration, whichDay);
    // Split strings and convert to ofText
    convertOFText();
}

//--------------------------------------------------------------
void ofTextSelect::update()
{
    if (isActive)
    {
        updateTextTransparency();
        updateTextHoverColor();
        //updateTextSelection();
    }

    else
    {
        resetTextTransparency();
    }

}

//--------------------------------------------------------------
void ofTextSelect::draw()
{

    for (int i = 0; i < textSelections.size(); i++)
    {


        ofSetColor(0,
                   0,
                   0,
                   textSelections[i].transVal);
        ofNoFill();
        ofSetLineWidth(100);
        defaultFont->drawString(
            textSelections[i].keywords,
            monitorPositionX + textSelections[i].initX,
            textSelections[i].initY
            );

        ofSetColor(textSelections[i].redVal,
                   textSelections[i].greenVal,
                   255,
                   textSelections[i].transVal);
        ofFill();
        defaultFont->drawString(
            textSelections[i].keywords,
            monitorPositionX + textSelections[i].initX,
            textSelections[i].initY
            );


    }



}

//--------------------------------------------------------------
void ofTextSelect::init(string filename, float vidDuration, int whichDay)
{
    videoDuration = vidDuration;

    fileName = filename;

    if(whichDay == 0){
        path = resources.getKeywordLocation()
            + fileName
            + resources.getKeywordFileFormat();
    }else {
        path = resources.getKeywordLocationDayTwo()
            + fileName
            + resources.getKeywordFileFormat();
    }


    defaultFont = resources.getFont();
}

//--------------------------------------------------------------
void ofTextSelect::startTimer()
{
    startTime = ofGetElapsedTimeMillis();

}

//--------------------------------------------------------------
void ofTextSelect::convertOFText()
{
    vector<string> strLines = getLinesFromFile();

    setOFText(strLines);
}

//--------------------------------------------------------------
vector<string> ofTextSelect::getLinesFromFile()
{
    ofBuffer buffer = ofBufferFromFile(path);
    string fullText = buffer.getText();

    vector<string> lines = ofSplitString(fullText, ">", true, true);

    return lines;
}

//--------------------------------------------------------------
void ofTextSelect::setOFText(vector<string> keywords)
{
    // Gets the area for showing the keywords.
    int keywordAreaWidth = resources.getBigPlayerWidth()*0.9;
    int keywordAreaHeight = resources.getBigPlayerHeight()*0.35;

    ofText temp;

    for (int i=1; i < keywords.size(); i+=2)
    {
        temp.keywords = keywords[i];
        temp.timestamp = ofToInt(keywords[i-1]);
        temp.initY = i/ceil(float((keywords.size()/3)))*(keywordAreaHeight-50)+ofRandom(-10,10)+75;
        temp.initX = (i%3)*(keywordAreaWidth-30)/3+ofRandom(-20,20)+100;
        temp.wordSelected = false;
        temp.greenVal = 255;
        temp.redVal = 255;
        temp.transVal = 0;
        temp.endTime = (int)ofRandom(4000, 30000);

        textSelections.push_back(temp);
    }
}

//--------------------------------------------------------------
void ofTextSelect::resetTextTransparency()
{
    for(int i=0; i < textSelections.size(); i++)
    {
        textSelections[i].transVal = 0;
    }
}

//--------------------------------------------------------------
void ofTextSelect::setActive(bool active)
{
    isActive = active;
}

//--------------------------------------------------------------
bool ofTextSelect::isOfTextSelectActive()
{
    return isActive;
}

//--------------------------------------------------------------
float ofTextSelect::getVideoTimeStamp(int counter)
{
    return textSelections[counter].timestamp;
}

//--------------------------------------------------------------
void ofTextSelect::updateTextTransparency()
{

    for(int i=0; i<textSelections.size(); i++)
    {

        int timer = ofGetElapsedTimeMillis() - startTime;
        //printf("Timer : %d End Time : %d\n ",timer,textSelections[i].endTime);
        if (timer > textSelections[i].endTime && textSelections[i].transVal < 255)
        {

            textSelections[i].transVal += 15;
        }
    }
}

//--------------------------------------------------------------
void ofTextSelect::setBigDisplayMonitor(int monitor)
{
    monitorPositionX = (monitor == 1) ? 0 : resources.getScreenHalfWidth();
}

//--------------------------------------------------------------
int ofTextSelect::findSelectedKeyword(int posX, int posY)
{

    for (int i=0; i < textSelections.size(); i++)
    {
        int tempStartX = textSelections[i].initX
                    + monitorPositionX;
        int tempEndX = textSelections[i].initX
                    + monitorPositionX
                    + defaultFont->stringWidth(textSelections[i].keywords);
        int tempStartY = textSelections[i].initY
                    - defaultFont->stringHeight(textSelections[i].keywords);

        if ( posX >= tempStartX
            && posX <= tempEndX
            && posY >= tempStartY
            && posY <= textSelections[i].initY
            && textSelections[i].transVal > 200)
        {
            return i;
        }

    }

    return -1;

}

//--------------------------------------------------------------
void ofTextSelect::updateTextHoverColor()
{
    for (int i=0 ; i < textSelections.size(); i++)
    {
        if(textSelections[i].wordSelected
           && textSelections[i].transVal > 25
           && textSelections[i].greenVal > 0)
        {
            textSelections[i].greenVal -= 15;
            //textSelections[i].redVal -= 15;
        }
        else if (!textSelections[i].wordSelected)
        {
            textSelections[i].greenVal = 255;
           // textSelections[i].redVal = 255;
        }
    }
}

//--------------------------------------------------------------
void ofTextSelect::setKeywordActive(bool active, int counter)
{
    textSelections[counter].wordSelected = active;
}

//--------------------------------------------------------------
void ofTextSelect::startKeywordTimer()
{
    startKeywordTime = ofGetElapsedTimeMillis();
}

//--------------------------------------------------------------
int ofTextSelect::getKeywordTime()
{
    return ofGetElapsedTimeMillis() - startKeywordTime;
}

//--------------------------------------------------------------
bool ofTextSelect::isOfTextSelectKeywordActive(int counter)
{
    return textSelections[counter].wordSelected;
}
