/*
*	Author	        :	Ruzette Tanyag
*                   :   Jessica Peter
*	Version Dates	:	July 11, 2013
*                    :   July 17, 2013
*                    :   August 25, 2013
*                    :   April 18, 2014
*	Title	:	ofNEWSManager.h
*	Comments:
*       [TO BE EDITED]
*
*/

//NOTE: Issues identified with using "draw as vector" in text. Esp. on initiating Pierre Trudeau doc and on upswipe.
//Attempted to fix by changing swipe up/down to handle transparency only (as seen in this version).
#ifndef OFNEWSMANAGER_H
#define OFNEWSMANAGER_H

#include "ofMain.h"

#include "ofNEWSResources.h"
#include "ofVid.h"
#include "ofTextSelect.h"


#include "../../../../addons/gestureTek/Include/GestTrack3DMulti.h"
//#include "GestTrack3DMulti.h"


class ofNEWSManager : public ofBaseApp
{

    //--------------------------------------------------------------
    // Variables and classes are declared here.
    public:

        ofImage                 dateIcon;

    private:
        ofNEWSResources         resources;
        ofTrueTypeFont          *defaultFont;
        GestTrack3DMultiData    tracker;


        int                     activePanel                           = 0;
        int                     selectedPanel                         = 0;
        int                     activeBigDisplayMonitor               = 0;         // 0 if none, 1 if left wall, 2 if right wall
        int                     activeSelectedText                    = -1;

        int                     whichDay                              = 0;

        float                   cursorX                               = 0;
        float                   cursorY                               = 0;
        int                     cursorDiameter                        = 20;
        bool                    cursorAnimGrow                        = false;

        ofVid*                  videoPanels[MAX_VIDEO_PANEL_COUNT];
        ofTextSelect*           textFromVideoPanels[MAX_VIDEO_PANEL_COUNT];

        vector<double>          smoothX;
        vector<double>          smoothY;

        int                     skip                                  = 0;
        int                     swipeCount                            = 0;
        int                     mX;
        int                     mY;

    //--------------------------------------------------------------
    //  Methods are declared here.
    public:

        void                    setup();
		void                    update();
		void                    draw();
		void                    mousePressed(int x, int y, int button);
		void                    mouseMoved(int x, int y );
		void                    keyPressed(int key);

    private:
        void                    initVideoPanels();
        void                    initTextFromVideoPanels();
        void                    initGestTrack3DMulti();

        void                    drawSmallVideoPanels();
        void                    drawBigVideoPanel();
        void                    updateVideoPanels();
        void                    exitBigDisplay();

        void                    processSelectedPanel(float posX, float posY);
        void                    updateCursorDiameter();
        void                    drawCursor();

        void                    updateCursorSelection();
        void                    updateTextFromVideo();
        void                    updateGestTracker();
        void                    drawTextFromVideo();

        void                    updateCursor(int posX, int posY);
        void                    processActivePanel(int posX, int posY);

        void                    processKeywordsFromVideo(int posX, int posY);

        float                   getSmoothValue(double value, vector<double> *smoothVal);

        void                    updateDayToggleWithGest();
        void                    toggleVideoDay();
        void                    drawToggleButton();


};

#endif // OFNEWSMANAGER_H
