#include "ofNEWSManager.h"
#include "ofNEWSResources.h"
#include "ofVid.h"

#include <iostream>
#include "../../../../addons/gestureTek/Include/GestTrack3DMulti.h"

//--------------------------------------------------------------
void ofNEWSManager::setup()
{

    ofSetLogLevel(OF_LOG_VERBOSE);

    dateIcon.loadImage("icon_1.png");

    ofBackground(0);
    initVideoPanels();
    initTextFromVideoPanels();

    initGestTrack3DMulti();
}

//--------------------------------------------------------------
void ofNEWSManager::update()
{
    updateGestTracker();
    updateCursorDiameter();
    updateCursorSelection();
    updateVideoPanels();
    updateTextFromVideo();

}

//--------------------------------------------------------------
void ofNEWSManager::draw()
{

    drawSmallVideoPanels();
    drawBigVideoPanel();
    drawTextFromVideo();
    drawCursor();
    drawToggleButton();

}

//--------------------------------------------------------------
// Continues Finding if GestureTek is connected
// before connecting to the system.
void ofNEWSManager::initGestTrack3DMulti()
{
    int result = GestTrack3DMulti_Start();

	while(result != 0)
    {
        result = GestTrack3DMulti_Start();
    }

    smoothX.assign(20, 0.0000);
    smoothY.assign(20, 0.0000);
}

//--------------------------------------------------------------
void ofNEWSManager::initVideoPanels()
{
    int counter = 0;

    string* filenames;

    if (whichDay == 0){
        filenames = resources.getFileNames();
    } else {
        filenames = resources.getFileNamesDayTwo();
    }

    for (int i = 0; i < resources.getPanelRows(); i++)
    {
        for(int j = 0; j < resources.getPanelColumns(); j++)
        {
            videoPanels[counter] = new ofVid(
                j*resources.getSmallPlayerWidth(),
                i*resources.getSmallPlayerHeight(),
                filenames[((counter)%MAX_NEWS_FILE_COUNT)].c_str(),
                whichDay
                );
            counter++;
        }
    }
}

//--------------------------------------------------------------
void ofNEWSManager::initTextFromVideoPanels()
{
    string* filenames;

    if(whichDay == 0){
        filenames = resources.getFileNames();
    }else {
        filenames = resources.getFileNamesDayTwo();
    }


    for (int i=0; i < MAX_VIDEO_PANEL_COUNT; i++)
    {
        textFromVideoPanels[i] = new ofTextSelect(
            filenames[i%MAX_NEWS_FILE_COUNT].c_str(),
            videoPanels[i]->getVideoDuration(),
            whichDay
            );
        textFromVideoPanels[i]->setBigDisplayMonitor(videoPanels[i]->getBigDisplayMonitor());
    }
}

//--------------------------------------------------------------
void ofNEWSManager::drawSmallVideoPanels()
{

    for(int i = 1; i < MAX_VIDEO_PANEL_COUNT; i++)
    {

        if (videoPanels[i]->getSmallDisplayMonitor() == activeBigDisplayMonitor
            && videoPanels[activePanel]->isBigPlayerActive())
                continue;

            videoPanels[i]->draw();
    }

    // TODO : Check why videoPanel[0] and
    // videoPanel[4] do not update on their own.
    // WTH ARE YOU NOT COOPERATING?
    // Don't ask. This is just the messy part.
    if (videoPanels[0]->getSmallDisplayMonitor() != activeBigDisplayMonitor)
    {
        videoPanels[0]->draw();
    }

    if (videoPanels[4]->getSmallDisplayMonitor() != activeBigDisplayMonitor)
    {
        videoPanels[4]->draw();
    }

}

//--------------------------------------------------------------
void ofNEWSManager::updateVideoPanels()
{

    for(int i = 0; i < MAX_VIDEO_PANEL_COUNT; i++)
    {
        videoPanels[i]->update();
    }

}

//--------------------------------------------------------------
void ofNEWSManager::mousePressed(int x, int y, int button)
{

    mX = x;
    mY = y;

    updateDayToggleWithGest();


    // Determines which panel location it is.
    /*
    int panelX = floor(x/resources.getSmallPlayerWidth());
    int panelY = floor(y/resources.getSmallPlayerHeight());
    int temp   = (panelX+(panelY*resources.getPanelColumns()));

    if (videoPanels[temp]->getSmallDisplayMonitor() != activeBigDisplayMonitor)
    {
        videoPanels[activePanel]->setBigPlayerActive(false);
        textFromVideoPanels[activePanel]->setActive(false);

        activePanel = temp;

        videoPanels[activePanel]->setBigPlayerActive(true);
        activeBigDisplayMonitor = videoPanels[activePanel]->getBigDisplayMonitor();

        textFromVideoPanels[activePanel]->setActive(true);
        textFromVideoPanels[activePanel]->startTimer();
    }
    */
}

//--------------------------------------------------------------
void ofNEWSManager::drawBigVideoPanel()
{
    if (videoPanels[activePanel]->isBigPlayerActive())
    {
        videoPanels[activePanel]->drawBigPanel();
    }
}

//--------------------------------------------------------------
void ofNEWSManager::keyPressed(int key)
{
    exitBigDisplay();
    /*
    switch(key)
    {
    case ' ':
        exitBigDisplay();
        break;
    case 't':
        bool temps = textFromVideoPanels[activePanel]->isOfTextSelectActive();
        textFromVideoPanels[activePanel]->setActive(!temps);
        break;
    }*/

}

//--------------------------------------------------------------
void ofNEWSManager::mouseMoved(int x, int y )
{
    //processSelectedPanel(x, y);
}

//--------------------------------------------------------------
void ofNEWSManager::exitBigDisplay()
{
    videoPanels[activePanel]->setBigPlayerActive(false);
    textFromVideoPanels[activePanel]->setActive(false);
    activeBigDisplayMonitor = 0;
}

//--------------------------------------------------------------
void ofNEWSManager::processSelectedPanel(float posX, float posY)
{
    int panelX = floor(posX/resources.getSmallPlayerWidth());
    int panelY = floor(posY/resources.getSmallPlayerHeight());

    int temp = (panelX+(panelY*resources.getPanelColumns()));

    if (selectedPanel != temp
        && selectedPanel < MAX_VIDEO_PANEL_COUNT
        && temp < MAX_VIDEO_PANEL_COUNT)
    {
        videoPanels[selectedPanel]->setActive(false);
        videoPanels[temp]->setActive(true);
        videoPanels[temp]->startTimer();

        selectedPanel = temp;
    }
    else if (selectedPanel != temp
        && selectedPanel <= MAX_VIDEO_PANEL_COUNT
        && temp >= MAX_VIDEO_PANEL_COUNT)
    {

        videoPanels[temp]->setStartAnimActive(false);
        videoPanels[selectedPanel]->setActive(false);
        selectedPanel = 0;
    }
}

//--------------------------------------------------------------
void ofNEWSManager::updateCursorDiameter()
{
    if ((videoPanels[selectedPanel]->hasStartAnim() && !cursorAnimGrow )
        || (textFromVideoPanels[activePanel]->isOfTextSelectKeywordActive(activeSelectedText) && !cursorAnimGrow))
    {
        if (cursorDiameter == 30)
        {
            cursorAnimGrow = true;
        }
        cursorDiameter++;
    }
    else if ((videoPanels[selectedPanel]->hasStartAnim() && cursorAnimGrow )
        || (textFromVideoPanels[activePanel]->isOfTextSelectKeywordActive(activeSelectedText) && cursorAnimGrow))
    {
        if (cursorDiameter == 20)
        {
            cursorAnimGrow = false;
        }
        cursorDiameter--;
    }
    else
    {
        cursorDiameter = 20;
    }
}

//--------------------------------------------------------------
void ofNEWSManager::drawCursor()
{
    ofSetCircleResolution(100);
    ofFill();
    ofSetColor(255,255,255,200);
    ofCircle(cursorX, cursorY, cursorDiameter, cursorDiameter);
    ofNoFill();
    ofSetColor(0,100,200,200);
    ofSetLineWidth(5.0);
    ofCircle(cursorX,cursorY, cursorDiameter, cursorDiameter);
    ofFill();


}
//----------------------------------------

void ofNEWSManager::processActivePanel(int posX, int posY)
{
	int panelX = floor(posX/resources.getSmallPlayerWidth());
	int panelY = floor(posY/resources.getSmallPlayerHeight());

	int temp = (panelX+(panelY*resources.getPanelColumns()));

	if (selectedPanel != temp
		&& selectedPanel < MAX_VIDEO_PANEL_COUNT
		&& temp < MAX_VIDEO_PANEL_COUNT)
	{
		videoPanels[selectedPanel]->setActive(false);
		videoPanels[temp]->setActive(true);
		videoPanels[temp]->startTimer();

		selectedPanel = temp;
	}
	else if (selectedPanel != temp
		&& selectedPanel <= MAX_VIDEO_PANEL_COUNT
		&& temp >= MAX_VIDEO_PANEL_COUNT)
	{

		videoPanels[temp]->setStartAnimActive(false);
		videoPanels[selectedPanel]->setActive(false);
		selectedPanel = 0;
	}
}

//--------------------------------------------------------------
void ofNEWSManager::updateCursor(int posX, int posY)
{
    cursorX = posX;
    cursorY = posY;

    if (videoPanels[selectedPanel]->hasStartAnim() && !cursorAnimGrow)
    {
        if (cursorDiameter <= 30)
        {
            cursorDiameter++;
        }
        else
        {
            cursorAnimGrow = true;
        }
    }
    else if (videoPanels[selectedPanel]->hasStartAnim() && cursorAnimGrow)
    {
        if (cursorDiameter >= 20)
        {
            cursorDiameter--;
        }
        else
        {
            cursorAnimGrow = false;
        }
    }
    else
    {
        cursorDiameter = 20;
    }
}

//--------------------------------------------------------------

//------------------------------------------
void ofNEWSManager::updateCursorSelection()
{

    if (activePanel == selectedPanel) return;

    if (videoPanels[selectedPanel]->isVideoPanelActive()
        && videoPanels[selectedPanel]->getTime() > resources.getCursorStartAnimationDuration()
        && videoPanels[selectedPanel]->getSmallDisplayMonitor() != activeBigDisplayMonitor)
    {
        videoPanels[selectedPanel]->setStartAnimActive(true);
    }


    if (videoPanels[selectedPanel]->hasStartAnim()
        && videoPanels[selectedPanel]->getTime() > resources.getCursorEndAnimationDuration()
        && videoPanels[selectedPanel]->getSmallDisplayMonitor() != activeBigDisplayMonitor)
    {
        videoPanels[selectedPanel]->setStartAnimActive(false);
        videoPanels[activePanel]->setBigPlayerActive(false);
        textFromVideoPanels[activePanel]->setActive(false);
        textFromVideoPanels[activePanel]->resetTextTransparency();

        activePanel = selectedPanel;

        videoPanels[activePanel]->setBigPlayerActive(true);
        activeBigDisplayMonitor = videoPanels[activePanel]->getBigDisplayMonitor();
        textFromVideoPanels[activePanel]->setActive(true);
        textFromVideoPanels[activePanel]->startTimer();
    }

    if (textFromVideoPanels[activePanel]->isOfTextSelectKeywordActive(activeSelectedText)
        && textFromVideoPanels[activePanel]->getKeywordTime() > resources.getKeywordEndAnimationDuration())
    {
        textFromVideoPanels[activePanel]->setKeywordActive(false, activeSelectedText);
        videoPanels[activePanel]->setVideoPosition(
            textFromVideoPanels[activePanel]->getVideoTimeStamp(activeSelectedText)
                );

        activeSelectedText = -1;
        textFromVideoPanels[activePanel]->resetTextTransparency();
        textFromVideoPanels[activePanel]->startTimer();
    }

}

//--------------------------------------------------------------
void ofNEWSManager::updateTextFromVideo()
{
    int temp;
    if (videoPanels[activePanel]->isBigPlayerActive())
    {
        textFromVideoPanels[activePanel]->update();
        processKeywordsFromVideo(cursorX, cursorY);
    }
}

//--------------------------------------------------------------
void ofNEWSManager::drawTextFromVideo()
{
    if (videoPanels[activePanel]->isBigPlayerActive())
    {

        textFromVideoPanels[activePanel]->draw();
    }

}

//--------------------------------------------------------------
void ofNEWSManager::updateGestTracker()
{
    // Get tracker data

    GestTrackGesture gestureData;
    GestTrackBody bodyData;

    GestTrack3DMulti_GetData(&tracker);

    gestureData = tracker.gesture;
    bodyData    = tracker.body;

    if(bodyData.hand1.hand2d.found == 1)
    {
        //printf("hand X : %f \t hand Y : %f \n", bodyData.hand1.hand2d.position.x, bodyData.hand1.hand2d.position.y);
        cursorX = ofMap(getSmoothValue(bodyData.hand1.hand2d.position.x+0.1, &smoothX),
                        0.0,
                        1.0,
                        0,
                        resources.getScreenWidth());
        //printf("Y\n");
        cursorY = ofMap(getSmoothValue(bodyData.hand1.hand2d.position.y+0.1, &smoothY),
                        0.0,
                        1.0,
                        0,
                        resources.getScreenHeight(), true);
        //printf("cursor X : %f \t cursor Y : %f \n", cursorX, cursorY);

    }

    if (skip = 2)
    {
        skip = 0;
        // Up Swipe
        if( gestureData.palm.found
           && (gestureData.swipe.direction > 40 && gestureData.swipe.direction < 140)
           && videoPanels[activePanel]->isBigPlayerActive()
           && swipeCount < gestureData.swipe.count)
        {
            bool temp = textFromVideoPanels[activePanel]->isOfTextSelectActive();
            textFromVideoPanels[activePanel]->setActive(!temp);
            swipeCount = gestureData.swipe.count;
        }


        if( gestureData.palm.found
           && videoPanels[activePanel]->isBigPlayerActive()
           && swipeCount < gestureData.swipe.count
           && ((gestureData.swipe.direction > 140 && gestureData.swipe.direction < 270)
            //|| (gestureData.swipe.direction >= 0 && gestureData.swipe.direction < 40)
            //|| (gestureData.swipe.direction > 320 && gestureData.swipe.direction < 360)
               ))
        {
            exitBigDisplay();
            swipeCount = gestureData.swipe.count;
        }
    }
    processSelectedPanel(cursorX, cursorY);
    ++skip;
}

//--------------------------------------------------------------
void ofNEWSManager::processKeywordsFromVideo(int posX, int posY)
{
    int temp = textFromVideoPanels[activePanel]->findSelectedKeyword(posX, posY);

    if (temp != activeSelectedText && temp != -1)
    {
        textFromVideoPanels[activePanel]->setKeywordActive(false, activeSelectedText);
        activeSelectedText = temp;
        textFromVideoPanels[activePanel]->setKeywordActive(true, activeSelectedText);
        textFromVideoPanels[activePanel]->startKeywordTimer();
    }
    else if (temp != activeSelectedText && temp == -1)
    {
        textFromVideoPanels[activePanel]->setKeywordActive(false, activeSelectedText);
        activeSelectedText = -1;
    }

}

//--------------------------------------------------------------
float ofNEWSManager::getSmoothValue(double value, vector<double> *smoothVal)
{
    double temp = 0;

    *smoothVal->erase(smoothVal->begin());
    smoothVal->push_back(value);

    for(int i = 0; i< smoothVal->size(); i++)
    {
        temp += smoothVal->at(i);
    }
    return (float)temp/smoothVal->size();
}

//--------------------------------------------------------------
void ofNEWSManager::updateDayToggleWithGest()
{


    ofLog(OF_LOG_VERBOSE,"cursorX" + ofToString(cursorX));
    ofLog(OF_LOG_VERBOSE,"cursorY" + ofToString(cursorY));
    ofLog(OF_LOG_VERBOSE,"mX" + ofToString(mX));
    ofLog(OF_LOG_VERBOSE,"mY" + ofToString(mY));


    if((cursorX > 50 && cursorX < 150 && cursorY > 50 && cursorY < 150)
       || (mX > 50 && mX < 150 && mY > 50 && mY < 150)){
        toggleVideoDay();
    }

}

//--------------------------------------------------------------
void ofNEWSManager::toggleVideoDay()
{

    string* filenames;

    if(whichDay == 0){
       whichDay = 1;
       filenames = resources.getFileNamesDayTwo();
    } else {
        whichDay = 0;
        filenames = resources.getFileNames();
    }

    ofLog(OF_LOG_VERBOSE,"Which Day?!" + ofToString(whichDay));

    for(int i = 0; i < MAX_VIDEO_PANEL_COUNT; i++)
    {
        videoPanels[i]->updateVideoFilename(filenames[((i)%MAX_NEWS_FILE_COUNT)].c_str(), whichDay);
    }

}

//--------------------------------------------------------------
void ofNEWSManager::drawToggleButton(){

    ofSetColor(255,255,255,250);
    if(whichDay == 0){
        dateIcon.loadImage("icon_1.png");
    } else {
        dateIcon.loadImage("icon_2_r-02.png");
    }
    ofRect(50, 50, 122, 65);
    dateIcon.draw(50, 50, 122, 65);
}


