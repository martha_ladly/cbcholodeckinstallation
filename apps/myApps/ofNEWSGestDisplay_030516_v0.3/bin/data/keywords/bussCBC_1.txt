            24>Christina Pochmursky >
            29>holiday edition >
            33>economy >
            35>developing country >
            40>new economy >
            53>job loss >
            55>bewilderment >
            60>shifting gears >
            71>Lance Secretan>
            80>fables >
            83>living the moment>
            88>Nuala Beck>
            95>old economy >
            109>declining>
            125>tough business cycle>
            148>post-war period>
            159>thousand year cycle>
            164>powerhouses>
            185>four powerful engines of growth>
            188>computer>
            189>semi conductor sector>
            195>electronic industry >
            198>pulp and paper industry>
            205>health and medical sectors>
            210>private sector >
            215>pharmaceutical >
            218>surgical instruments>
            235>communication and telecommunication sector>
            245>mining and petroleum industry>
            260>instrumentation industry >
            276>weatherman>
            315>industrial age >
            318>1981>
            390>implications>
            409>employment>
            497>average pay >
            502>percentage of women >
            578>the New Age>
            650>IBM>
            700>projections>
            710>2030>
            716>next powerful transitions>
            729>space commerce>
            733>NASA>