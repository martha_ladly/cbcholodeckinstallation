Jan 1, 2007 Holodeck files

MONICA
1--INVU Courtney Love

00:01:32:29 – survivor
00:01:44:00 - self-esteem
00:02:00:00 - diaries
00:02:11:00 – mess
00:02:11:00 - confessions
00:02:21:00 – rehab
00:02:21:00 - validation
00:02:27:00 - ridicule
00:02:36:15 - Dirty Blonde
00:02:44:22 – fans
00:02:50:04 – fascinating
00:02:50:04 - record


LAURA
2--NEWS Saddam Hussein death
•	00:04:48:08
•	00:07:28:14

MONICA
3--NEWS Stephen Harper - security 
•	00:08:24:16
•	00:09:56:18

00:08:25:20 – security breach
00:08:25:20 - car
00:08:30:23 – Stephen Harper
00:08:38:08 - RCMP   
00:08:42:17 – gate
00:08:42:17 - woman
00:08:44:27 – intercepted
00:09:02:00 – custody
00:09:05:00 – charges
00:09:09:15 – risk
00:09:23:12 – security measures
00:09:39:18 - review

LAURA
4--NEWS NYE in Afghanistan
•	00:09:56:18
•	00:12:25:00

MONICA
5--NEWS Smoking ban
•	00:12:48:22
•	00:14:36:26

00:12:48:22 – Calgary
00:12:54:00 - smoke-free
00:13:05:22 – restaurants 
00:13:15:20 – ashtrays
00:13:20:23 – by-law
00:13:25:01 – investigate
00:13:26:22 – officers
00:13:31:02 – complaint-driven 
00:13:45:22 – bingo halls 
00:14:00:02 – shisha
00:14:08:28 – cultural exemption
00:14:25:10 - loopholes




LAURA
9--FEATURE Pianist competition
•	00:32:42:06
•	00:59:03:18



LAURA
11-AD Verizon computers with Steve Nash
•	01:00:15:18
•	01:01:15:20

12-FEATURE 49 Up
•	01:04:51:26
•	01:14:12:14
•	(break)
•	01:18:12:22
•	01:30:17:12

13-FEATURE Absolutely Canadian
•	02:18:20:10
•	02:27:12:26

MONICA
14-NEWS Fashion File
•	02:30:49:00
•	02:34:17:08

02:31:30:00 - Tom Ford
02:31:30:00 - vulgarity
02:31:26:37 - menswear
02:31:45:10 - Saint Laurent
02:32:01:00 - plums
02:32:24:00 – summer
02:32:50:00 – The 70’s
02:32:58:03 – boulevardier
02:32:58:03 – European
02:33:19:05 - silhouette
02:33:24:00 - geeky
02:33:24:00 - bow ties
02:33:47:00 - resigned
02:33:58:10 - love


LAURA
15-NEWS Fire Fest Windsor
•	02:34:49:08
•	02:41:45:20

MONICA
16-NEWS General update
•	04:03:26:18
•	04:05:02:00

04:03:42:00 – Happy New Year
04:03:49:00 – Times Square
04:03:53:20 - Afghanistan
04:03:59:00 – fireworks
04:03:59:00 - beach
04:04:06:00 - European Union
04:04:12:29 - Somalia
04:04:19:12 – fighter jets
04:04:24:28 - Mogadishu
04:04:29:12 - insurgents 
04:04:35:00 - Calgary
04:04:38:29 – smoking
04:04:32:05 - Canada

MONICA
17-FEATURE Nature of Things - Stephen Lewis
•	04:05:02:00
•	05:02:58:04

04:05:18:20 – pandemic
04:06:19:17 – poverty
04:07:35:00 – hero
04:07:59:29 – Africa
04:08:18:00 – United Nations
04:08:32:20 – progress
04:17:45:00 – frantic
04:22:58:00 – women
04:28:00:02 – orphan
04:35:44:20 - impotence 
04:59:46:27 - grotesque
05:00:59:21 – Special Envoy


18-FEATURE The 5th Estate
•	06:03:33:24
•	06:59:05:06

19-AD Lexmark printers
•	07:00:25:06
•	07:00:57:06

LAURA
20-OTHER Canada Heritage Moment
•	07:02:17:10
•	07:03:37:12

21-FEATURE The Lens - Rock and Roll Kid
•	07:04:57:14
•	07:58:52:22