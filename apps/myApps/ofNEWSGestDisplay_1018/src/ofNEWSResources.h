/*
*	Author	        :	Ruzette Tanyag
*                   :   Jessica Peter
*	Version Dates	:	July 11, 2013
*                   :   July 17, 2013
*                   :   August 25, 2013
*                   :   April 18, 2014
*	Title	        :	ofNEWSResources.h
*	Comments        :
*
*/

#ifndef OFNEWSRESOURCES_H
#define OFNEWSRESOURCES_H

#include    <string>
#include    <iostream>

#include    "ofMain.h"



// TODO : Dynamically set maximum file counts and video panel counts
#define     MAX_VIDEO_PANEL_COUNT       24
#define     MAX_NEWS_FILE_COUNT         13


class ofNEWSResources
{
    //--------------------------------------------------------------
    // Variables and classes are declared here.
    public:


    private:
        string              fontName                                = "trebuc.ttf";
        int                 fontDpi                                 = 72;
        int                 fontSize                                = 36;
        float               fontLineHeight                          = 40.0f;
        float               fontLetterSpacing                       = 1.035;



        ofTrueTypeFont*     defaultFont;
        string              NEWSFilenames[MAX_NEWS_FILE_COUNT]      = {
                                                                    "adCBC_1",
                                                                    "bonCBC_1",
                                                                    "bussCBC_1",
                                                                    "czeCBC_1",
                                                                    //"docCBC_1",
                                                                    "entCBC_1",
                                                                    "fashionCBC_1",
                                                                    "historyCBC_1",
                                                                    "journalCBC_1",
                                                                    "newsAcross2CBC_1",
                                                                    "newsAcrossCBC_1",
                                                                    "newsCBC_1",
                                                                    "nwCBC_1",
                                                                    "planetCBC_1"
                                                                        };

        string              videoLocation                           = "movies/";
        string              videoFileFormat                         = ".3gp";

        string              keywordLocation                         = "keywords/";
        string              keywordFileFormat                       = ".txt";

//////////////////////////////////RESOLUTION////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// mach 20th
//1600 X 1200
// 3200 x 1200


        int                 smallPlayerWidth                        = 400; //(1024/4=256) total width / 8// 1400/4=350
        int                 smallPlayerHeight                       = 400; // (768/3)screenwidth /1.33// 1050/3=350
        int                 bigPlayerWidth                          = 0;
        int                 bigPlayerHeight                         = 0;

        int                 screenWidth                             = 3200;//(1024x2) (1400x2=2800)
        int                 screenHeight                            = 1200;
        int                 screenHalfWidth                         = 1600;

        int                 panelRows                               = 3;
        int                 panelColumns                            = 8;

        int                 cursorDiameter                          = 20;
        int                 cursorStartAnimDuration                 = 1500;//cursor set times?
        int                 cursorEndAnimDuration                   = 4000;//cursor set times?

        int                 keywordEndAnimDuration                  = 1500;

    //--------------------------------------------------------------
    //  Methods are declared here.
    public:
                            ofNEWSResources();
        string*             getFileNames();

        ofTrueTypeFont*     getFont();

        string              getVideoLocation();
        string              getVideoFileFormat();

        string              getKeywordLocation();
        string              getKeywordFileFormat();

        void                setSmallPlayerHeight(int height);
        void                setSmallPlayerWidth(int width);


        int                 getBigPlayerWidth();
        int                 getBigPlayerHeight();
        int                 getSmallPlayerWidth();
        int                 getSmallPlayerHeight();

        int                 getMonitorPosX(int posX);
        int                 getMonitorPosY();

        int                 getScreenWidth();
        int                 getScreenHeight();
        int                 getScreenHalfWidth();

        int                 getPanelRows();
        int                 getPanelColumns();

        int                 getMonitorFromPosition(int posX);

        int                 getCursorDiameter();
        int                 getCursorStartAnimationDuration();
        int                 getCursorEndAnimationDuration();

        int                 getKeywordEndAnimationDuration();

    private:
        void                initFont();
};

#endif // OFNEWSRESOURCES_H
