

#include "ofMain.h"
#include "ofNEWSResources.h"


struct ofText
{
        string                          keywords;
        int                             timestamp;
        int                             initX;
        int                             initY;
        bool                            wordSelected;
        int                             greenVal;
        int                             redVal;
        int                             transVal;
        int                             endTime;
};

class ofTextSelect
{
    //--------------------------------------------------------------
    // Variables and classes are declared here.
    public:
        ofNEWSResources                 resources;

    private:
        ofTrueTypeFont                  *defaultFont;

        vector<ofText>                  textSelections;

        float                           videoDuration;
        string                          fileName;
        string                          path;

        bool                            isBigDisplayRunning             = false;
        bool                            isActive                        = false;
        int                             monitorPositionX                = 0;
        int                             startTime;
        int                             startKeywordTime;

    //--------------------------------------------------------------
    //  Methods are declared here.
    public:
                                        ofTextSelect(string filename, float vidDuration);

        void                            update();
        void                            draw();
        void                            startTimer();
        void                            startKeywordTimer();

        void                            setActive(bool active);
        void                            setBigDisplayMonitor(int monitor);
        void                            setKeywordActive(bool active, int counter);

        bool                            isOfTextSelectActive();
        bool                            isOfTextSelectKeywordActive(int counter);

        float                           getVideoTimeStamp(int counter);
        int                             findSelectedKeyword(int posX, int posY);

        int                             getKeywordTime();

        void                            resetTextTransparency();


    private:
        void                            init(string filename, float vidDuration);
        void                            setOFText(vector<string> keywords);

        void                            convertOFText();

        void                            updateTextTransparency();
        void                            updateTextHoverColor();

        vector<string>                  getLinesFromFile();

};
