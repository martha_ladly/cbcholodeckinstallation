#include "ofNEWSResources.h"
#include "ofMain.h"

//--------------------------------------------------------------
ofNEWSResources::ofNEWSResources()
{
    initFont();
}

//--------------------------------------------------------------
string* ofNEWSResources::getFileNames()
{
    return NEWSFilenames;
}

//--------------------------------------------------------------
// Returns the ofTrueTypeFont
ofTrueTypeFont*  ofNEWSResources::getFont()
{
    return defaultFont;
}

//--------------------------------------------------------------
// Initializes the font
void ofNEWSResources::initFont()
{
    ofTrueTypeFont::setGlobalDpi(fontDpi);
	defaultFont = new ofTrueTypeFont();

	defaultFont->loadFont(fontName, fontSize, true, true, true);
	defaultFont->setLineHeight(fontLineHeight);
	defaultFont->setLetterSpacing(fontLetterSpacing);

}

//--------------------------------------------------------------
string ofNEWSResources::getVideoLocation()
{
    return videoLocation;
}

//--------------------------------------------------------------
string ofNEWSResources::getVideoFileFormat()
{
    return videoFileFormat;
}

//--------------------------------------------------------------
string ofNEWSResources::getKeywordLocation()
{
    return keywordLocation;
}

//--------------------------------------------------------------
string ofNEWSResources::getKeywordFileFormat()
{
    return keywordFileFormat;
}

//--------------------------------------------------------------
void ofNEWSResources::setSmallPlayerWidth(int width)
{
    smallPlayerWidth = width;

}

//--------------------------------------------------------------
void ofNEWSResources::setSmallPlayerHeight(int height)
{
    smallPlayerHeight = height;
}

//--------------------------------------------------------------
int ofNEWSResources::getSmallPlayerWidth()
{
    return smallPlayerWidth;
}

//--------------------------------------------------------------
int ofNEWSResources::getSmallPlayerHeight()
{
    return smallPlayerHeight;
}

//--------------------------------------------------------------
int ofNEWSResources::getBigPlayerHeight()
{
    bigPlayerHeight = getSmallPlayerHeight()*3;
    return bigPlayerHeight;
}

//--------------------------------------------------------------
int ofNEWSResources::getBigPlayerWidth()
{
    bigPlayerWidth = getScreenWidth()/2;
    return bigPlayerWidth;
}

//--------------------------------------------------------------
// TODO : Must make dynamic.
int ofNEWSResources::getMonitorPosX(int posX)
{
    if (posX < ((int)(getScreenWidth()/2)))
        return ((int)(getScreenWidth()/2));
    return 0;
}

//--------------------------------------------------------------
int ofNEWSResources::getMonitorPosY()
{
    return 0;
}

//--------------------------------------------------------------
int ofNEWSResources::getScreenWidth()
{
    return screenWidth;
}

//--------------------------------------------------------------
int ofNEWSResources::getScreenHeight()
{
    return screenHeight;
}

//--------------------------------------------------------------
int ofNEWSResources::getPanelRows()
{
    return panelRows;
}

//--------------------------------------------------------------
int ofNEWSResources::getPanelColumns()
{
    return panelColumns;
}

//--------------------------------------------------------------
int ofNEWSResources::getScreenHalfWidth()
{
    return screenHalfWidth;
}

//--------------------------------------------------------------
int ofNEWSResources::getMonitorFromPosition(int posX)
{
    if (posX < getScreenHalfWidth())
        return 1;
    return 2;
}

//--------------------------------------------------------------
int ofNEWSResources::getCursorDiameter()
{
    return cursorDiameter;
}

//--------------------------------------------------------------
int ofNEWSResources::getCursorStartAnimationDuration()
{
    return cursorStartAnimDuration;
}

//--------------------------------------------------------------
int ofNEWSResources::getCursorEndAnimationDuration()
{

    return cursorEndAnimDuration;
}

//--------------------------------------------------------------
int ofNEWSResources::getKeywordEndAnimationDuration()
{
    return keywordEndAnimDuration;
}
