#include "ofMain.h"
#include "ofAppGlutWindow.h"
#include "ofAppGLFWWindow.h"

#include "ofNEWSManager.h"
#include "ofNEWSResources.h"

#define SCREENWIDTH     3360
#define SCREENHEIGHT    1050

//========================================================================
int main()
{
    ofAppGLFWWindow win;

    win.setMultiDisplayFullscreen(true);

    ofSetupOpenGL(&win,SCREENWIDTH, SCREENHEIGHT,OF_FULLSCREEN);
    ofSetWindowTitle("CBC Newsworld Holodeck");

	ofRunApp(new ofNEWSManager());
    //ofAppGlutWindow window;


	//ofSetupOpenGL(&window, SCREENWIDTH, SCREENHEIGHT,OF_WINDOW);			// <-------- setup the GL context
	//ofSetWindowTitle("CBC Newsworld Holodeck");
	//ofRunApp(new ofNEWSManager());
}
