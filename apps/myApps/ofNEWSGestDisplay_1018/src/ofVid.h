/*
*	Author	        :   Jessica Peter
*
*	Version Dates	:   August 25, 2013
*                    :   April 18, 2014
*	Title	:	ofVid.h
*	Comments:
*          [TO EDIT]
*/

#include <iostream>
#include "ofMain.h"

#include "ofNEWSResources.h"

#ifndef OFVID_H
#define OFVID_H



class ofVid
{
    //--------------------------------------------------------------
    // Variables and classes are declared here.
    public:

    private:
        int                 posX;
        int                 posY;
        int                 bigDisplayMonitor;

        string              fileName;

        ofVideoPlayer       videoPlayer;    //TODO : Look into QTVideoPlayer

        float               duration;

        bool                isActive;
        bool                isSmallPlayerEnabled;
        bool                isBigPlayerEnabled;

        bool                isStartAnim;

        float               startTime;

        ofNEWSResources     resources;

    //--------------------------------------------------------------
    //  Methods are declared here.
    public:
                            ofVid(int locX, int locY, string videoFilename);
                            ofVid(int locX, int locY);
        void                update();
        void                draw();

        void                drawBigPanel();
        void                setActive(bool active);
        void                setSmallPlayerActive(bool active);
        void                setBigPlayerActive(bool active);
        void                setStartAnimActive(bool active);

        bool                isBigPlayerActive();
        bool                isSmallPlayerActive();
        bool                isVideoPanelActive();

        int                 getBigDisplayMonitor();
        int                 getSmallDisplayMonitor();

        float               getVideoDuration();

        bool                hasStartAnim();

        void                startTimer();
        float               getTime();

        void                setVideoPosition(float percentage);


    private:
        void                init(int locX, int locY, string videoFilename);
        void                initVideoPlayer();
        void                initPlayerEnabler();
        void                drawPanel();
        void                setBigDisplayMonitor();



};

#endif // OFVID_H
