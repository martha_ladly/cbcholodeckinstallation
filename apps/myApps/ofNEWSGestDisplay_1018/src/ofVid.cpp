#include "ofVid.h"
#include "ofNEWSResources.h"

#include "ofMain.h"

//--------------------------------------------------------------
ofVid::ofVid(int locX, int locY, string videoFilename)
{
    init(locX, locY, videoFilename);
}

//--------------------------------------------------------------
ofVid::ofVid(int locX, int locY)
{
    init(locX, locY,"");
}

//--------------------------------------------------------------
// update method, used to refresh your objects properties
void ofVid::update()
{

    if(isSmallPlayerEnabled)
    {
        videoPlayer.update();
    }

    if(isBigPlayerEnabled)
    {
        videoPlayer.update();
    }
}

//--------------------------------------------------------------
// draw method, this where you'll do the object's drawing
void ofVid::draw()
{
    drawPanel();

    if(isSmallPlayerEnabled && isActive && !isBigPlayerEnabled)
    {
        videoPlayer.setVolume(1.0);
    }
    else if (isSmallPlayerEnabled && !isActive && !isBigPlayerEnabled)
    {
        ofEnableAlphaBlending();
        ofSetColor(0,0,0,150);
        ofRect(
               resources.getMonitorPosX(posX),
               resources.getMonitorPosY(),
               resources.getSmallPlayerWidth(),
               resources.getSmallPlayerHeight()
               );
        videoPlayer.setVolume(0.0);
    }

}

//--------------------------------------------------------------
void ofVid::init(int locX, int locY, string videoFilename)
{

    posX       = locX;
    posY       = locY;
    fileName   = videoFilename;

    initVideoPlayer();

    initPlayerEnabler();

    setBigDisplayMonitor();
}

//--------------------------------------------------------------
void ofVid::initVideoPlayer()
{
    string path = resources.getVideoLocation() + fileName + resources.getVideoFileFormat();

    ofSetVerticalSync(true);

    videoPlayer.loadMovie(path);
    videoPlayer.play();
    videoPlayer.setVolume(0.0);



    duration = videoPlayer.getDuration();
}

void ofVid::initPlayerEnabler()
{
    isActive = false;
    isSmallPlayerEnabled = true;
    isBigPlayerEnabled = false;
    isStartAnim = false;
}
//--------------------------------------------------------------
void ofVid::setActive(bool active)
{
    isActive = active;
}

//--------------------------------------------------------------
void ofVid::setSmallPlayerActive(bool active)
{
    isSmallPlayerEnabled = active;
}

//--------------------------------------------------------------
void ofVid::setBigPlayerActive(bool active)
{
    isBigPlayerEnabled = active;
}

//--------------------------------------------------------------
//draw movies
//set color to white to display properly
void ofVid::drawPanel()
{
	ofSetColor(255,255,255);
	ofFill();
	videoPlayer.draw(posX, posY, resources.getSmallPlayerWidth(), resources.getSmallPlayerHeight());
}

//--------------------------------------------------------------
void ofVid::drawBigPanel()
{
    ofSetColor(255,255,255);
	ofFill();
    videoPlayer.draw(
            resources.getMonitorPosX(posX),
            resources.getMonitorPosY(),
            resources.getBigPlayerWidth(),
            resources.getBigPlayerHeight()
            );
    videoPlayer.setVolume(1.0);
}

//--------------------------------------------------------------
bool ofVid::isBigPlayerActive()
{
    return isBigPlayerEnabled;
}

//--------------------------------------------------------------
bool ofVid::isSmallPlayerActive()
{
    return isSmallPlayerEnabled;
}

//--------------------------------------------------------------
bool ofVid::isVideoPanelActive()
{
    return isActive;
}

//--------------------------------------------------------------
int ofVid::getBigDisplayMonitor()
{
    return bigDisplayMonitor;
}

//--------------------------------------------------------------
void ofVid::setBigDisplayMonitor()
{
    int monitorPosX = resources.getMonitorPosX(posX);

    switch(monitorPosX)
    {
        /////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //////RESOLUTION////////////////////////////////////////////////
        case 0:
            bigDisplayMonitor = 1;
            break;
        case 1600://width of 1 screen
            bigDisplayMonitor = 2;
            break;
        default:
            bigDisplayMonitor = 1;
    }
}

//--------------------------------------------------------------
int ofVid::getSmallDisplayMonitor()
{
    return resources.getMonitorFromPosition(posX);
}

//--------------------------------------------------------------
float ofVid::getVideoDuration()
{
    return duration;
}

//--------------------------------------------------------------
bool ofVid::hasStartAnim()
{
    return isStartAnim;
}

//--------------------------------------------------------------
void ofVid::setStartAnimActive(bool active)
{
    isStartAnim = active;
}

//--------------------------------------------------------------
void ofVid::startTimer()
{
    startTime = ofGetElapsedTimeMillis();
}

//--------------------------------------------------------------
float ofVid::getTime()
{
    return ofGetElapsedTimeMillis() - startTime;
}

//--------------------------------------------------------------
void ofVid::setVideoPosition(float percentage)
{
    videoPlayer.setPosition(percentage/duration);
}
