// ===========================================================================
// GestTrack3DHand.h
// 2010, GestureTek.  All Rights Reserved.
//
//
// ===========================================================================

#ifndef INC_GESTURETEK_GESTTRACK3DHAND
#define INC_GESTURETEK_GESTTRACK3DHAND

// ===========================================================================
// Typedefs, defines and enums ===============================================
// ===========================================================================

// A simple 3D point
struct GestTrack3DPoint
{
  double x;
  double y;
  double z;
};

// Tracking data for a single Hand
struct GestTrack3DData
{
  // Unique ID of the hand
  int m_ID;
  // X,Y,Z Absolute Position of the hand, relative to the camera (measured in millimeters)
  GestTrack3DPoint m_Pos_Absolute;
  // X,Y,Z Normalized Position of the hand, relative to the camera (in the range [0.0, 1.0])
  GestTrack3DPoint m_Pos_Normalized;
};

// A region in 3D space within which all tracking is taking place, as specified by the GestTrack3D Tracker.
// This region is defined in millimeters, relative to the camera's position & orientation.
struct GestTrack3DVolume
{
  GestTrack3DPoint m_NearPlane[4];  // [Xmin, Ymin, Zmin],[Xmax, Ymin, Zmin],[Xmax,Ymax,Zmin],[Xmin,Ymax,Zmin]
  GestTrack3DPoint m_FarPlane[4];   // [Xmin, Ymin, Zmax],[Xmax, Ymin, Zmax],[Xmax,Ymax,Zmax],[Xmin,Ymax,Zmax]
};


// ===========================================================================
// Function prototypes =======================================================
// ===========================================================================

#ifdef GESTTRACK3D_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif

#ifdef __cplusplus
#define EXTC   extern "C"
#else 
#define EXTC  
#endif 


// ===========================================================================
// Function:    
//  GestTrack3DHand_Start
//
// Description: 
//  Connects to the running GestTrack3D Tracker. This function must be called
//  before any calls to GestTrack3D_GetData()
//
// Params: None    
//
// Returns: 0 on success, non-zero on failure.
// ===========================================================================
EXTC DLL_API int  GestTrack3DHand_Start( void );


// ===========================================================================
// Function:
//  GestTrack3DHand_Stop
//
// Description:
//  Disconnects from the GestTrack3D Tracker. This function should be called 
//  prior to the application shutting down.
//  Has no effect if GestTrack3D_Start has not been called.
//
// Params: None
// ===========================================================================
EXTC DLL_API void GestTrack3DHand_Stop ( void );


// ===========================================================================
// Function:    
//  GestTrack3DHand_GetData
//
// Description: 
//  Retrieves the most recent data from the GestTrack3D Tracker. Has no effect
//  if GestTrack3D_Start() has not been successfully called.
//
// Params:      
//  pData: 
//    An application-defined array of GestTrack3DData structures. The elements
//    of this array will be filled with information about each hand currently
//    being tracked.
//  pVolume:
//    A pointer to an application-defined GestTrack3DVolume structure. This
//    structure will be filled with information about the 3D volume currently
//    defined by the GestTrack3D tracker, within which all hand tracking is
//    currently taking place. The m_Pos_Absolute member of each tracked hand
//    will be within this volume.
//  piSize: 
//    Input: The number of elements in the pData array
//    Output: The number of hands currently being tracked
//
// Returns: 
//  The number of structures within pData that were filled with tracked hand data.
// ===========================================================================
EXTC DLL_API int GestTrack3DHand_GetData(
  GestTrack3DData*    pData,
  GestTrack3DVolume*  pVolume,
  int*                piSize
  );


#endif // INC_GESTURETEK_GESTTRACK3DHAND
