// ===========================================================================
// GestTrack3DMulti.h
// 2010, GestureTek.  All Rights Reserved.
//
//
// ===========================================================================

#ifndef INC_GESTURETEK_GESTTRACK3DMULTI
#define INC_GESTURETEK_GESTTRACK3DMULTI

// ===========================================================================
// Typedefs, defines and enums ===============================================
// ===========================================================================

struct GestTrack2DPoint
{
  double x;
  double y;
};

struct GestTrack3DPoint
{
  double x;
  double y;
  double z;
};


// GestTrackOutlines
//
// Describes the outlines or silhouettes of all the different objects found 
// by the tracker. Each silhouette represents one object, and consists of an
// array of 2d points that describes the outline of the silhouette.
//
// The application is responsible for allocating memory for each of the
// silhouettes it wishes to track. It should allocate a separate array of
// GestTrack2DPoint structures for each silhouette, and set the 'capacity'
// member to the size of the array. It also needs to declare an array
// of GestTrackSilhouette structures to hold the silhouette points, and
// assign this array to the 'silhouettes' member, and set the capacity.
//
struct GestTrackSilhouette
{
  int count;
  int capacity;
  GestTrack2DPoint* points;
};

struct GestTrackOutlines
{
  int count;
  int capacity;
  GestTrackSilhouette* silhouettes;
};


// GestTrackBody
//
// Describes the position and orientation of different parts of
// the body. Each structure captures one part of the body.
// The found member will be non-zero when the tracker detects
// that part of the body.
//
struct GestTrackHand2D
{
  int found;
  GestTrack3DPoint position;
};
struct GestTrackHand3D
{
  int found;
  GestTrack3DPoint position;
};
struct GestTrackHand
{
  GestTrackHand2D hand2d;
  GestTrackHand3D hand3d;
};

struct GestTrackHead
{
  int found;
  GestTrack3DPoint pos3d;
  GestTrack2DPoint pos2d;
};

struct GestTrackTorso
{
  int found;
  GestTrack2DPoint pos2d;
  GestTrack3DPoint pos3d;
  GestTrack3DPoint angle;
};

struct GestTrackBody
{
  GestTrackHand  hand1;
  GestTrackHand  hand2;
  GestTrackHand  fourPntTkrHand1;
  GestTrackHand  fourPntTkrHand2;
  GestTrackHead  head;
  GestTrackTorso torso;
};


// GestTrackGesture
//
// Describes any gestures that have been detected by the 
// tracker. Each structure represents one type of gesture that
// can be tracked.
//
struct GestTrackCircle
{
  int found;
  double relAngle;
  double absAngle;
};

struct GestTrackSwipe
{
  int count;
  double direction;
};

struct GestTrackPoke
{
  int count;
};

struct GestTrackRotate
{
  int found;
  double angle;
};

struct GestTrackScale
{
  int found;
  double amount;
};

struct GestTrackPalm
{
  int found;
};

struct GestTrackGesture
{
  GestTrackCircle circle;
  GestTrackSwipe  swipe;
  GestTrackPoke   poke;
  GestTrackRotate rotate;
  GestTrackScale  scale;
  GestTrackPalm   palm;
};


struct GestTrack3DMultiData
{
  GestTrackOutlines outlines;
  GestTrackBody     body;
  GestTrackGesture  gesture;
};


// ===========================================================================
// Function prototypes =======================================================
// ===========================================================================

#ifdef GESTTRACK3D_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif

#ifdef __cplusplus
#define EXTC   extern "C"
#else 
#define EXTC  
#endif 


// ===========================================================================
// Function:    
//  GestTrack3DMulti_Start
//
// Description: 
//  Connects to the running GestTrack 3D Multi Tracker. This function must be
//  called before any calls to GestTrack3DMulti_GetData()
//
// Params: None    
//
// Returns: 0 on success, non-zero on failure.
// ===========================================================================
EXTC DLL_API int  GestTrack3DMulti_Start( void );


// ===========================================================================
// Function:
//  GestTrack3DMulti_Stop
//
// Description:
//  Disconnects from the GestTrack 3D MultiTracker. 
//  This function should be called prior to the application shutting down.
//  Has no effect if GestTrack3DMulti_Start has not been called.
//
// Params: None
// ===========================================================================
EXTC DLL_API void GestTrack3DMulti_Stop ( void );


// ===========================================================================
// Function:    
//  GestTrack3DMulti_GetData
//
// Description: 
//  Retrieves the most recent data from the GestTrack 3D Multi Tracker.
//  Has no effect if GestTrack3DMulti_Start() has not been successfully called.
//
// Params:      
//  pData: 
//    
// ===========================================================================
EXTC DLL_API void GestTrack3DMulti_GetData( GestTrack3DMultiData* pData );


#endif // INC_GESTURETEK_GESTTRACK3DMULTI
